package hw4;

import java.util.Scanner;


public class Homework4 {
	
	public static void main( String[] args ) {		
		
		String clientName;
		int clientAge;
		double loanAmount;
		
		try (Scanner scanner = new Scanner(System.in)) {
			System.out.print("input your name: ");
			clientName = scanner.nextLine();
			System.out.print("input your age: ");
			clientAge = scanner.nextInt();
			System.out.print("input credit amount: ");
			loanAmount = scanner.nextDouble();
		} 
		
		if (!clientName.equals("Bob") && 
			clientAge >= 18 &&
			loanAmount <= clientAge * 100) {
			
			System.out.println("loan is approved!");
		} else {
			System.out.println("sorry, loan is not approved");
		}
				
	}
}
