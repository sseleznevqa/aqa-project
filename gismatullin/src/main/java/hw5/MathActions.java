package hw5;

enum MathActions {
	PLUS ("+"),
	MINUS ("-"),
	MULTI ("*"),
	DIVIDE ("/");
	
	private final String sign;

    private MathActions(String sign) {
        this.sign = sign;
    }
    
    String getSign() {
    	return this.sign;
    }
}