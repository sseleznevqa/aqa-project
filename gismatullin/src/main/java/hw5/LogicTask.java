package hw5;

import java.util.Random;
import java.util.Scanner;


public class LogicTask {
	
	private static double[] mainRow; 				// main row (progression)
	private static MathActions[] mathActionsRow;	// array of math actions (steps) for progression
	private static int[] numbersForActionsRow; 		// array of numbers (steps) for progression
	
	private static Random random = new Random();
	private static Scanner scanner;
	
	public static void main(String[] args) {
		
		scanner = new Scanner(System.in);
		int difficultyLevel = getLevel();
		getTask(difficultyLevel); // create random task row
//		printTask(true); // show hint for testing
		
		System.out.println("You need to guess last number of a progression.");
		printTask(false); // print task row w/o last element
		
		int triesIsLeft = 3;
		
		while(triesIsLeft > 0) {
			System.out.printf("You have %d tries.\n", triesIsLeft);
			System.out.print("Please input a number: ");
			int num = scanner.nextInt();
			if (num == mainRow[mainRow.length - 1]) {
				System.out.println("Congratulations, You win!"); // user has inputed right number
				break;
			} else {
				triesIsLeft--;	// user has inputed wrong number
				System.out.print("Sorry, You are wrong. ");
			}
		}
		
		if (triesIsLeft == 0) { 	// Print message and task after 3 wrong tries
			System.out.println("Right progression was: ");
			printTask(true); //print full row
		}
		scanner.close();
	}

	
	// get difficulty level
	private static int getLevel() {
		
		System.out.println("This logic task have three difficulty levels:\n"
				 + "1 - EASY, 2 - NORMAL and 3 - HARD");
		System.out.print("Please input number of level 1, 2 or 3: ");
		return scanner.nextInt();
	}

	
	// create random task row
	private static void getTask(int difficultyLevel) {
		
		int mainRowLength = 0; // init
		int rowSteps = 0; // init
		
		// length of task progression depends on difficulty level
		switch (difficultyLevel) {
			case 1 : {
				mainRowLength = 5 + random.nextInt(2); // easy task
				rowSteps = 1;
				break;
			}
			case 2 : {
				mainRowLength = 6 - random.nextInt(2); // normal task
				rowSteps = 2;
				break;
			}
			case 3 : {
				mainRowLength = 10 - random.nextInt(4); // hard task
				rowSteps = 3;
				break;
			}
		}
		
		// let's create task
		do {
			mainRow = new double[mainRowLength];	 // create array of main row
			mathActionsRow = new MathActions[rowSteps]; // create array of math actions (for steps)
			numbersForActionsRow = new int[rowSteps]; // // create array of numbers for steps
			randomFillStepRows();
			randomFillMainRow();
		} while (!checkMainRow()); // iterate until get right task

	}
	
	
	private static void randomFillStepRows() {
		// bounds for +, - is 1..19
		int boundForStepNumber1 = 19;
		// bounds for *, / is 2..3
		int boundForStepNumber2 = 3;
		
		// filling signs and numbers for progression steps
		for (int i = 0; i < mathActionsRow.length; i++) {
			int act = random.nextInt(MathActions.values().length); // random values in range 0..3
			mathActionsRow[i] = MathActions.values()[act];
			if (act == MathActions.PLUS.ordinal() || act == MathActions.MINUS.ordinal()) {
				numbersForActionsRow[i] = random.nextInt(boundForStepNumber1) + 1; // for + and -
			} else {
				numbersForActionsRow[i] = random.nextInt(boundForStepNumber2 - 1) + 2; // for * and /
			}
		}
	}
	
	
	// calculating and filling main row
	private static void randomFillMainRow() {
		
		double currValue = 0; // just init
		// correct first element by first math action
		switch(mathActionsRow[0]) { 
			case  PLUS : {
				currValue = random.nextInt(80);
				break;
			}
			case MINUS : {
				currValue = (int) random.nextInt(50) * 2;
				break;
			}
			case MULTI : {
				currValue = random.nextInt(6);
				break;
			}
			case DIVIDE : {
				currValue = random.nextInt(5) * 16;
				break;
			}
		}
		mainRow[0] = currValue;
		
		// fill remaining elements of progression
		for (int i = 1; i < mainRow.length;) {
			for (int j = 0; j < mathActionsRow.length && i < mainRow.length; j++) {
				currValue = mathOps(currValue, numbersForActionsRow[j], mathActionsRow[j]);
				mainRow[i++] = currValue;
			}
		}
	}

	
	// check progression for according rules
	private static boolean checkMainRow() {
		double minPossibleNumber = 1.0;
		double maxPossibleNumber = 99.0;
		double prevNum = -1;
		for (double num : mainRow) {
			if (num < minPossibleNumber || num > maxPossibleNumber 
					|| num != (int) num || num == prevNum) {
				return false;
			}
			prevNum = num;
		}
		return true;
	}
	
	
	// function for calculating result of math actions
	private static double mathOps(double a, double b, MathActions action) {
		
		switch (action) {
			case PLUS : return a + b;
			case MINUS : return a - b;
			case MULTI : return a * b;
			case DIVIDE : {
				if (b != 0) {
					return a / b;
				} 
			}
			default :
				return 0;
		}
	}
	
	
	// print full or incomplete row
	private static void printTask(boolean printFullRow) {
		
		for(int i = 0; i < mainRow.length - 1; i++) { // print task row w/o last element
			System.out.printf("%d\t", (int) mainRow[i]);
		}
		if (printFullRow) {
			// print last element of task row
			System.out.println((int) mainRow[mainRow.length - 1]); 
			
			// print row of math actions (steps)
			for(int i = 0; i < (mainRow.length - 1);) {
				for(int j = 0; j < mathActionsRow.length && i < (mainRow.length - 1); j++, i++) {
					System.out.printf("   %s%s\t", mathActionsRow[j].getSign(),numbersForActionsRow[j]);
				}
			}
			System.out.println();
		} else {
			// print incomplete main row
			System.out.println("?"); 
		}
	}
}

