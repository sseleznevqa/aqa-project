import java.util.Scanner;

public class LoansProcessorProgram {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Введите имя: ");
        String enteringName = sc.nextLine();

        System.out.println("Введите возраст: ");
        int enteringAge = sc.nextInt();

        System.out.println("Введите сумму которую вы хотите получить в кредит: ");
        double enteringAmount = sc.nextDouble();

        loansProcessor(enteringName,enteringAge,enteringAmount);


    }

    public static void loansProcessor(String name, int age, double amount) {

        if (!"Bob".equals(name) && age >= 18 && amount <= age * 100) {
            System.out.println("Кредит выдан!");
        } else {
            System.out.println("К сожалению мы не можем выдать вам кредит");
        }
    }
}
